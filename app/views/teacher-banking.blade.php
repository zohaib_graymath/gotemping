@extends('layout/main')

@section('content')

<!-- header -->
@include('includes.subHeader')
<!-- header -->



<div class="container-fluid">
	<div class="container">
    	<div class="row main-content">
        
          
      <!-- left-nav -->
        @include('includes.leftNav')    
        <!-- left-nav -->


            
            <!-- right-content -->
            <div class="right-content pull-right">
            	
                <!--medical-check-->
            	<div class="medical-check right-content-inner">
                	<form class="form-horizontal reference-form" role="form">
                    	<h1>Provide banking information</h1>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">Bank name</label>
                          <div class="col-sm-7">
                             <input type="text" class="form-control" id="firstname" placeholder="Example Bank of Australia">
                          </div>
                       </div>
                       <div class="form-group twoInputs">
                          <label for="firstname" class="col-sm-3 control-label">BSB</label>
                          <div class="col-sm-3">
                             <input type="text" class="form-control" id="firstname" placeholder="000000">
                          </div>
                          <div class="slash">-</div>
                          <div class="col-sm-3">
                             <input type="text" class="form-control" id="firstname" placeholder="000000">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">Account number</label>
                          <div class="col-sm-7">
                             <input type="text" class="form-control" id="firstname" placeholder="0123456789">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">Account name</label>
                          <div class="col-sm-7">
                             <input type="text" class="form-control" id="firstname" placeholder="John Smith">
                          </div>
                       </div>
                    </form>
                    
                </div>
                <!--medical-check-->
                
            </div>
            <!-- right-content -->
            
            
            <!-- right-content -->
            <div class="right-content pull-right">
            	
                <!--medical-check-->
            	<div class="medical-check right-content-inner">
                	<form class="form-horizontal reference-form" role="form">
                    	<h1>Provide details of your Superannuation fund</h1>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">Fund name</label>
                          <div class="col-sm-7">
                             <input type="text" class="form-control" id="firstname" placeholder="John.smith">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">Member number</label>
                          <div class="col-sm-7">
                             <input type="text" class="form-control" id="firstname" placeholder="0012345">
                          </div>
                       </div>
                       
                    </form>
                    
                    
                </div>
                <!--medical-check-->
                      <div class="save-cancel-box">
                      	<a class="save-btn" href="javascript:void(0);" >Save and continue</a>
                      	<a href="javascript:void(0);" >Cancel</a>
                        <div class="clearfix"></div>
                      </div>
                
            </div>
            <!-- right-content -->
            
        </div>
    </div>
</div>
  


@stop