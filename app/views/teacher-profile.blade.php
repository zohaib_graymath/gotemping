@extends('layout/main')

@section('content')


<!-- header -->
@include('includes.subHeader')
<!-- header -->


<div class="container-fluid">
	<div class="container">
    	<div class="row main-content">
            
            <!-- teacher-profile -->
            <div class="teacher-profile">
            	
                <!--profile-pic-->
            	<div class="profile-pic pull-left"></div>
                <!--profile-pic-->
                
                <!--tech-basic-detail-->
                <div class="tech-basic-detail pull-left">
                	<ul class="detail-list">
                    	<li><h3>John Smith</h3></li>
                        <li>Taught here before: <span>Yes</span></li>
                        <li>Years teaching: <span>6 years</span></li>
                        <li>Distance away: <span>3 km</span></li>
                        <li class="add-margin-bottom">Rating: <span>9/10</span></li>
                        <li>Teaches: <span>Primary</span></li>
                        <li>Classes:<span> P -  2, 3 - 4, 5 - 6</span></li>
                        <li>Extra Skills: <span>Music, L.O.T.E (Italian) </span></li>
                    </ul>
                </div>
                <!--tech-basic-detail-->
            
            	<!--right-btns-->
            	<div class="profile-btns pull-right">
                	<a href="javascript:void(0);" >Your hisotry</a>
                    <a href="javascript:void(0);" >Public comments</a>
                    <a class="disabled-link" href="javascript:void(0);" >Report teacher</a>
                </div>
                <!--right-btns-->
                
            <div class="clearfix"></div>
            </div>
            <!-- teacher-profile -->
            
            <!-- teacher-profile -->
            <div class="teacher-profile">
            	
                <!--left-section-->
            	<div class="left-section pull-left">
	                <h1>Qualifications & Teaching Hisotry</h1>
                    <div class="Qua-detail pull-left">
                    	<ul class="Qua-detail-list">
                        	<li><div>Do you have a current VIT Card?</div> <div>Yes</div></li>
                        	<li><div>VIT Registration Numer</div> <div>326172</div></li>
                        	<li><div>Initial Registration Date</div> <div>05 / 02 / 2014</div></li>
                        	<li class="add-margin-bottom"><div>Expiry date</div> <div>05 / 02 / 2015</div></li>
                        	<li><div>Do you have a current Criminal Record Check?</div> <div>No</div></li>
                        	<li><div>Do you have a current Work with Children card?</div> <div>Yes</div></li>
                        </ul>
                       <div class="divider"></div>
                       
                    	<ul class="Qua-detail-list">
                        	<li><div>Universtiy / Institution Attended</div> <div>RMIT University</div></li>
                        	<li><div>Degree obtained</div> <div>Masters of Education</div></li>
                        	<li><div>Date of graduation</div> <div>03 / 03 / 2010</div></li>
                        	<li><div>Years of Teaching Experience</div> <div>4</div></li>
                        	<li><div>Years of Senior Experience</div> <div>N / A</div></li>
                        </ul>
                        
                       <div class="divider"></div>
                        <h3>Last teaching position</h3>
                        
                        <ul class="Qua-detail-list">
                            <li><div>School</div><div>St Jude’s</div></li>
                            <li><div>Start date</div><div>10 / 04 / 2014</div></li>
                            <li><div>End date</div><div>29 / 02 / 2015</div></li>
                            <li><div>Class you taught</div><div>Grade 3</div></li>
                            <li><div>Address</div><div>53 Franks Road,</div></li>
                            <li><div>Telephone</div><div>Langwarrin, VIC 3208</div></li>
                            <li><div>Email</div><div>(03) 9564 4258</div></li>
                        </ul>
                        <div class="divider"></div>
                        <ul class="Qua-detail-list">
                        	<li><div>Have you ever been dismissed, demoted or <br>fired from any child-orientated workplace?</div><div>No</div></li>
                        	<li><textarea placeholder="please provide details of all instances" class="form-control textarea"></textarea></li>
                        </ul>
                    	 <div class="divider"></div>
                         <h3>Certificates & licences</h3>
                         <ul class="Qua-detail-list">
                          	<li><div>First Aid</div><div>No</div></li>
                          	<li><div>Mini Bus Licence</div><div>Yes</div></li>
                         </ul>
                         <div class="divider"></div>
                         <ul class="Qua-detail-list">
                          	<li><div>Are you qualified to teach a language <br>other the English</div><div>Yes</div></li>
                          	<li class="add-margin-bottom"><div>Langauge one</div><div>Italian</div></li>
                          	<li><div>Language two</div><div>N / A</div></li>
                         </ul>
                         
                    </div>
                </div>
                <!--left-section-->
                
                <!--right-section-->
            	<div class="right-section pull-right">
                	<div class="Qua-detail2 pull-left">
                    	<h1>Uploaded Documents</h1>
                        <ul class="Qua-detail-list">
                            <li><div>VIT Card</div><div><a class="show-btn" href="javascript:void(0);" >Show</a></div></li>
                            <li><div>100 points of ID</div><div></div>
                            	<ul>
                                	<li><div>Primary Document</div><div><a class="show-btn" href="javascript:void(0);" >Show</a></div></li>
                                	<li><div>	Secondary Document</div><div><a class="show-btn" href="javascript:void(0);" >Show</a></div></li>
                                </ul>
                            </li>
                            <li><div>Criminal Record Check</div><div><a class="show-btn not-file" href="javascript:void(0);" >No file</a></div></li>
                            <li><div>Working with Children card</div><div><a class="show-btn" href="javascript:void(0);" >Show</a></div></li>
                            <li><div>Resume</div><div><a class="show-btn" href="javascript:void(0);" >Show</a></div></li>
                            <li><div>Medical Certificates (if applicable)</div><div><a class="show-btn not-file" href="javascript:void(0);" >No file</a></div></li>
                        </ul>
                        <div class="divider"></div>
                        <h3>Referees</h3>
                        <h4>Referees</h4>
                        <ul class="Qua-detail-list">
                        	<li><div>School</div><div>St Jude’s</div></li>
                        	<li><div>Start date</div><div>10 / 04 / 2014</div></li>
                        	<li><div>End date</div><div>29 / 02 / 2015</div></li>
                        	<li><div>Name of Referee</div><div>Peter Edwards</div></li>
                        	<li><div>Title / Position</div><div>Vice Principle</div></li>
                        	<li><div>Address</div><div>53 Franks Road,Langwarrin, VIC 3208</div></li>
                        	<li><div>Telephone</div><div>(03) 9564 4258</div></li>
                        	<li><div>Email</div><div>p.edwards@stjudes.com.au</div></li>
                        </ul>
                         <h3>Referee Two</h3>
                        <ul class="Qua-detail-list">
                        	<li><div>School</div><div>St Jude’s</div></li>
                        	<li><div>Start date</div><div>10 / 04 / 2014</div></li>
                        	<li><div>End date</div><div>29 / 02 / 2015</div></li>
                        	<li><div>Name of Referee</div><div>Peter Edwards</div></li>
                        	<li><div>Title / Position</div><div>Vice Principle</div></li>
                        	<li><div>Address</div><div>53 Franks Road,Langwarrin, VIC 3208</div></li>
                        	<li><div>Telephone</div><div>(03) 9564 4258</div></li>
                        	<li><div>Email</div><div>p.edwards@stjudes.com.au</div></li>
                        </ul>
                        <div class="divider"></div>
                        <h3>Medical Check</h3>
                        <ul class="Qua-detail-list">
                        	<li><div>Exisiting injury or condition?</div><div>YesStress leave - 2 weeks in April 2010</div></li>
                        </ul>
                    </div>
                
                </div>
                <!--right-section-->
                <div class="clearfix"></div>
            </div>
            <!-- teacher-profile -->
            
        </div>
    </div>
</div>
  

@stop