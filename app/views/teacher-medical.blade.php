@extends('layout/main')

@section('content')

<!-- header -->
@include('includes.subHeader')
<!-- header -->



<div class="container-fluid">
	<div class="container">
    	<div class="row main-content">
        
      
      <!-- left-nav -->
        @include('includes.leftNav')    
        <!-- left-nav -->

            
            
            <!-- right-content -->
            <div class="right-content pull-right">
            	
                <!--medical-check-->
            	<div class="medical-check right-content-inner">
                	<div class="check-it">
                        <div class="checkbox">
                          <div class="col-sm-12 checkBoxOuter">
                            <div class="checkBox"></div>
                            <div class="checkBoxText">Are you aware of any circumstances regarding your health or capacity to work that would interfere with your ability to perform all tasks and duties effectively in a given teaching day including any current injury or condition or pre - existing injury or condition?</div>
                          </div>
                        
                           <!--<label><input class="check-box" type="checkbox">Are you aware of any circumstances regarding your health or capacity to work that would interfere with your ability to perform all tasks and duties effectively in a given teaching day including any current injury or condition or pre - existing injury or condition?</label>-->
                        </div>
                    </div>
                    <div class="clearfix"></div>
                      <div class="form-group">
                            <textarea placeholder="please provide details such as: existing or exposure to infectious diseases; medication/treatment required on a regular basis; or any current or past Work Cover claims." class="form-control textarea" rows="3"></textarea>
                      </div>
                </div>
                <!--medical-check-->
                      <div class="save-cancel-box">
                      	<a class="save-btn" href="javascript:void(0);" >Save and continue</a>
                      	<a href="javascript:void(0);" >Cancel</a>
                        <div class="clearfix"></div>
                      </div>
                
            </div>
            <!-- right-content -->
            
        </div>
    </div>
</div>
  

@stop