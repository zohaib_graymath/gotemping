@extends('layout/main')

@section('content')

	<!-- header -->
    @include('includes.subHeader')
    <!-- header -->
    


<!-- primary overlay -->
<div id="primary_overlay" class="Overlay" style="display:none;">
	<div class="container-fluid">
    	
        <div class="special-edu-performance">
        	<div class="popup-form-inner edu-form">
            	<h1>Primary Preferences</h1>
                <p>I can teach:</p>
					<form class="form-horizontal reference-form quals-form" role="form">
                    	<div class="form-group">
                          <div class="col-sm-10 checkBoxOuter">
                            <div class="checkBox"></div>
                            <div class="checkBoxText">P - 2</div>
                          </div>
                       </div>

                    	<div class="form-group">
                          <div class="col-sm-10 checkBoxOuter">
                            <div class="checkBox"></div>
                            <div class="checkBoxText">3 - 4</div>
                          </div>
                       </div>
                    	<div class="form-group add-margin-bottom">
                          <div class="col-sm-10 checkBoxOuter">
                            <div class="checkBox"></div>
                            <div class="checkBoxText">5 - 6</div>
                          </div>
                       </div>
                       <p>Special Classes</p>  
                    	<div class="form-group">
                          <div class="col-sm-10 checkBoxOuter">
                            <div class="checkBox"></div>
                            <div class="checkBoxText">Art</div>
                          </div>
                       </div>
                    	<div class="form-group">
                          <div class="col-sm-10 checkBoxOuter">
                            <div class="checkBox"></div>
                            <div class="checkBoxText">Music</div>
                          </div>
                       </div>
                    	<div class="form-group">
                          <div class="col-sm-10 checkBoxOuter">
                            <div class="checkBox"></div>
                            <div class="checkBoxText">P.E</div>
                          </div>
                       </div>
                    	<div class="form-group">
                          <div class="col-sm-10 checkBoxOuter">
                            <div class="checkBox"></div>
                            <div class="checkBoxText">L.O.T.E (1 per row)</div>
                          </div>
                       </div>
                    	<div class="form-group margin-top">
                          <div class="col-sm-12">
                          <input type="text" class="form-control" id="firstname" placeholder="Example">
                          </div>
                       </div>
                    	<div class="form-group">
                          <div class="col-sm-12">
                          <input type="text" class="form-control" id="firstname" placeholder="Example">
                          </div>
                       </div>
                       <div class="form-group">
                          <div class="col-sm-10 checkBoxOuter">
                            <div class="checkBox"></div>
                            <div class="checkBoxText">Other</div>
                          </div>
                       </div>
                    	<div class="form-group margin-top">
                          <div class="col-sm-12">
                          <input type="text" class="form-control" id="firstname" placeholder="Example">
                          </div>
                       </div>
                    	<div class="form-group">
                          <div class="col-sm-12">
                          <input type="text" class="form-control" id="firstname" placeholder="Example">
                          </div>
                       </div>

                    </form>   
                               
            </div>
            <div class="save-cancel-box">
                <a class="save-btn" href="javascript:void(0);">Save</a>
                <a class="cancel_btn" href="javascript:void(0);">Cancel</a>
                <div class="clearfix"></div>
            </div>               
        </div> 
	        
    </div>
</div>
<!-- primary overlay -->

<!-- special edu overlay -->
<div id="special_overlay" class="Overlay" style="display:none;">
	<div class="container-fluid">
    	<div class="special-edu-performance">
        	<div class="popup-form-inner edu-form">
            	<h1>Special Education Preferences</h1>
                <p>I can teach:</p>
					<form class="form-horizontal reference-form quals-form" role="form">
                    	<div class="form-group">
                          <div class="col-sm-10 checkBoxOuter">
                            <div class="checkBox"></div>
                            <div class="checkBoxText">Early Years</div>
                          </div>
                       </div>

                    	<div class="form-group">
                          <div class="col-sm-10 checkBoxOuter">
                            <div class="checkBox"></div>
                            <div class="checkBoxText">Juniors</div>
                          </div>
                       </div>
                    	<div class="form-group add-margin-bottom">
                          <div class="col-sm-10 checkBoxOuter">
                            <div class="checkBox"></div>
                            <div class="checkBoxText">Seniors</div>
                          </div>
                       </div>
                       <p>Capabilities</p>  
                    	<div class="form-group">
                          <div class="col-sm-10 checkBoxOuter">
                            <div class="checkBox"></div>
                            <div class="checkBoxText">Manual Handling</div>
                          </div>
                       </div>
                    	<div class="form-group">
                          <div class="col-sm-10 checkBoxOuter">
                            <div class="checkBox"></div>
                            <div class="checkBoxText">Toileting</div>
                          </div>
                       </div>
                    	<div class="form-group">
                          <div class="col-sm-10 checkBoxOuter">
                            <div class="checkBox"></div>
                            <div class="checkBoxText">Nappies</div>
                          </div>
                       </div>
                    	<div class="form-group">
                          <div class="col-sm-10 checkBoxOuter">
                            <div class="checkBox"></div>
                            <div class="checkBoxText">Other</div>
                          </div>
                       </div>
                    	<div class="form-group margin-top">
                          <div class="col-sm-12">
                          <input type="text" class="form-control" id="firstname" placeholder="Example">
                          </div>
                       </div>
                    	<div class="form-group">
                          <div class="col-sm-12">
                          <input type="text" class="form-control" id="firstname" placeholder="Example">
                          </div>
                       </div>
                    </form>   
                               
            </div>
            <div class="save-cancel-box">
                <a class="save-btn" href="javascript:void(0);">Save</a>
                <a class="cancel_btn" href="javascript:void(0);">Cancel</a>
                <div class="clearfix"></div>
            </div>               
        </div> 
         
	        
    </div>
</div>
<!-- special edu overlay -->




<!-- secondary edu overlay -->











<div id="secondary_overlay" class="Overlay" style="display:none">
	<div class="container-fluid">
	        <div class="container popup-form-wrap">
        	<div class="popup-form pull-right">
            	<div class="popup-form-inner">
            	<h1>Secondary Preferences</h1>
                <p>I can teach:</p>
                    <form class="form-horizontal" role="form">
                       <div class="form-group">
                          <div class="col-sm-10 checkBoxOuter">
                            <div class="checkBox"></div>
                            <div class="checkBoxText">Senior Level (V.C.E) 11 - 12</div>
                          </div>
                       </div>
                
                    </form>                
					<div class="inputs-wrap">
                    	<div class="inputs-left-list pull-left">
                         
                              <div class="checkBoxOuter">
                                <div class="checkBox"></div>
                                <div class="checkBoxText">ARTS - Art</div>
                              </div>
                              <div class="checkBoxOuter">
                                <div class="checkBox"></div>
                                <div class="checkBoxText">ARTS - Art</div>
                              </div>
                              <div class="checkBoxOuter">
                                <div class="checkBox"></div>
                                <div class="checkBoxText">ARTS - Art</div>
                              </div>
                              <div class="checkBoxOuter">
                                <div class="checkBox"></div>
                                <div class="checkBoxText">ARTS - Art</div>
                              </div>
                              <div class="checkBoxOuter">
                                <div class="checkBox"></div>
                                <div class="checkBoxText">ARTS - Art</div>
                              </div>
                              <div class="checkBoxOuter">
                                <div class="checkBox"></div>
                                <div class="checkBoxText">ARTS - Art</div>
                              </div>
                              <div class="checkBoxOuter">
                                <div class="checkBox"></div>
                                <div class="checkBoxText">ARTS - Art</div>
                              </div>
                              <div class="checkBoxOuter">
                                <div class="checkBox"></div>
                                <div class="checkBoxText">ARTS - Art</div>
                              </div>
                              <div class="checkBoxOuter">
                                <div class="checkBox"></div>
                                <div class="checkBoxText">ARTS - Art</div>
                              </div>
                              <div class="checkBoxOuter">
                                <div class="checkBox"></div>
                                <div class="checkBoxText">ARTS - Art</div>
                              </div>
                        	
                        </div>
                    	<div class="inputs-left-list right-div pull-right">
                        
                              <div class="checkBoxOuter">
                                <div class="checkBox"></div>
                                <div class="checkBoxText">ARTS - Art</div>
                              </div>
                              <div class="checkBoxOuter">
                                <div class="checkBox"></div>
                                <div class="checkBoxText">ARTS - Art</div>
                              </div>
                              <div class="checkBoxOuter">
                                <div class="checkBox"></div>
                                <div class="checkBoxText">ARTS - Art</div>
                              </div>
                              <div class="checkBoxOuter">
                                <div class="checkBox"></div>
                                <div class="checkBoxText">ARTS - Art</div>
                              </div>
                              <div class="checkBoxOuter">
                                <div class="checkBox"></div>
                                <div class="checkBoxText">ARTS - Art</div>
                              </div>
                              <div class="checkBoxOuter">
                                <div class="checkBox"></div>
                                <div class="checkBoxText">ARTS - Art</div>
                              </div>
                              <div class="checkBoxOuter">
                                <div class="checkBox"></div>
                                <div class="checkBoxText">ARTS - Art</div>
                              </div>
                              <div class="checkBoxOuter">
                                <div class="checkBox"></div>
                                <div class="checkBoxText">ARTS - Art</div>
                              </div>
                              <div class="checkBoxOuter">
                                <div class="checkBox"></div>
                                <div class="checkBoxText">ARTS - Art</div>
                              </div>
                              <div class="checkBoxOuter">
                                <div class="checkBox"></div>
                                <div class="checkBoxText">ARTS - Art</div>
                              </div>
                        	
                        </div>
                    </div>
                    <form class="form-horizontal reference-form" role="form">
                       <div class="form-group">
                          <div class="col-sm-10 checkBoxOuter">
                            <div class="checkBox"></div>
                            <div class="checkBoxText">Other (Add any subjects not listed)</div>
                          </div>
                       </div>
                       <div class="form-group">
                          <div class="col-sm-6">
                             <input type="text" class="form-control" id="firstname" placeholder="Example">
                          </div>
                       </div>
                       <div class="form-group">
                          <div class="col-sm-6">
                             <input type="text" class="form-control" id="firstname" placeholder="Example">
                          </div>
                       </div>
                    </form>                
                    </div>
                    <div class="save-cancel-box">
                      	<a class="save-btn" href="javascript:void(0);">Save and continue</a>
                      	<a class="cancel_btn" href="javascript:void(0);">Cancel</a>
                        <div class="clearfix"></div>
                    </div>                    
            </div>
        </div>
    </div>
</div>
<!-- secondary edu overlay -->


















<div class="container-fluid">
	<div class="container">
    	<div class="row main-content">
        
        <!-- left-nav -->
        @include('includes.leftNav')    
        <!-- left-nav -->

            
            
            <!-- right-content -->
            <div class="right-content pull-right">
            	
                <!--medical-check-->
            	<div class="medical-check right-content-inner">
                	<form class="form-horizontal reference-form quals-form" role="form">
                    	<h1>Where do you want to teach? </h1>
                       <div class="form-group">
                          <div class="col-sm-7">
                             <input type="text" class="form-control" id="firstname" placeholder="Address, Suburb, or Postcode">
                          </div>
                           <div class="dropdown dropdown-wrap">
                              <button class="btn btn-default dropdown-toggle dropdown-btn" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                Travel distance
                                <span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
                              </ul>
                           </div>  
                           <a href="javascript:void(0);" class="search-wrap"><span ></span></a>
                                                    	
                       </div>
                       
                    </form>
                    
                    
                    
                </div>
                <!--medical-check-->
                
                	<!--map-content-area-->
					<div class="map-content-area">
                    <div class="map-left-content pull-left">
                        <form class="form-horizontal reference-form teacher-preference-form" role="form">
                            <h1>Where do you want to teach? </h1>
                           <div class="form-group">
                              <div class="col-sm-10 checkBoxOuter">
                                <div class="checkBox"></div>
                                <div class="checkBoxText">Primary</div>
                                <a id="edit_primary" class="link-btn Edit" href="javascript:void(0);" >Edit</a>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-10 checkBoxOuter">
                                <div class="checkBox"></div>
                                <div class="checkBoxText">Secondary (7-10)</div>
                                <a id="edit_seconday" class="link-btn Edit" href="javascript:void(0);" >Edit</a>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-10 checkBoxOuter">
                                <div class="checkBox"></div>
                                <div class="checkBoxText">Secondary (V.C.E)</div>
                                <a class="link-btn Edit" href="javascript:void(0);" >Edit</a>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-10 checkBoxOuter last-child">
                                <div class="checkBox"></div>
                                <div class="checkBoxText">Special Education</div>
                                <a id="edit_special" class="link-btn Edit" href="javascript:void(0);" >Edit</a>
                              </div>
                           </div>
                            <h1>Schools you want to teach at</h1>
                           <div class="form-group">
                              <div class="col-sm-10 checkBoxOuter">
                                <div class="checkBox"></div>
                                <div class="checkBoxText">Government</div>
                                <a class="link-btn Edit" href="javascript:void(0);" >Edit</a>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-10 checkBoxOuter">
                                <div class="checkBox"></div>
                                <div class="checkBoxText">Catholic</div>
                                <a class="link-btn Edit" href="javascript:void(0);" >Edit</a>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-10 checkBoxOuter">
                                <div class="checkBox"></div>
                                <div class="checkBoxText">Independent</div>
                                <a class="link-btn Edit" href="javascript:void(0);" >Edit</a>
                              </div>
                           </div>
                            
                        </form>
                    </div>
                    <div class="right-map pull-right"><img src="assets/images/small-map.jpg" /></div>
                    </div>
                    <!--map-content-area-->
                    <div class="clearfix"></div>
                    
                      <div class="save-cancel-box">
                      	<a class="save-btn" href="javascript:void(0);" >Save and continue</a>
                      	<a href="javascript:void(0);" >Cancel</a>
                        <div class="clearfix"></div>
                      </div>
                     
            </div>
            <!-- right-content -->
            
        </div>
    </div>
</div>
  
@stop
