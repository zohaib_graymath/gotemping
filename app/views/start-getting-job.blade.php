@extends('layout/main')

@section('content')

<!-- header -->
@include('includes.subHeader')
<!-- header -->


<div class="container-fluid">
	<div class="container">
    	<div class="row main-content">
        
             
      <!-- left-nav -->
        @include('includes.leftNav')    
        <!-- left-nav -->
            
            
            
            <!-- right-content -->
            <div class="right-content pull-right">
            	
                <!--medical-check-->
            	<div class="medical-check right-content-inner">
                    <div class="approval-div">
                        <h3>Download the app </h3>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent hendrerit nibh dui. Curabitur nibh elit, venenatis sed nisi ut, auctor porta justo. Donec massa nunc, bibendum in metus et, tempor pharetra nisl. Praesent lacinia purus ac aliquet efficitur. Duis viverra velit eget augue venenatis auctor.</p>
                    </div>
                      
                </div>
                <!--medical-check-->
                      <div class="save-cancel-box">
                      	<a class="app_btn" href="javascript:void(0);" ><img src="assets/images/google-play.png" /></a>
                      	<a class="app_btn" href="javascript:void(0);" ><img src="assets/images/itune.png" /></a>
                        <div class="clearfix"></div>
                      </div>
                
            </div>
            <!-- right-content -->
            <!-- right-content -->
            <div class="right-content pull-right">
            	
                <!--medical-check-->
            	<div class="medical-check right-content-inner">
                    <div class="approval-div">
                        <h3>Run test notifications </h3>
                      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent hendrerit nibh dui. Curabitur nibh elit, venenatis sed nisi ut, auctor porta justo. Donec massa nunc, bibendum in metus et, tempor pharetra nisl. Praesent lacinia purus ac aliquet efficitur. Duis viverra velit eget augue venenatis auctor.</p>
                    </div>
                      
                </div>
                <!--medical-check-->
                      <div class="save-cancel-box">
                      	<a class="save-btn run_test" href="javascript:void(0);" >Run test</a>
                        <div class="clearfix"></div>
                      </div>
                
            </div>
            <!-- right-content -->
            
        </div>
    </div>
</div>
  

@stop
