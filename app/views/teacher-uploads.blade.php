@extends('layout/main')

@section('content')

<!-- header -->
@include('includes.subHeader')
<!-- header -->


<div class="container-fluid">
	<div class="container">
    	<div class="row main-content">
        
         
      <!-- left-nav -->
        @include('includes.leftNav')    
        <!-- left-nav -->

            
            
            
            <!-- right-content -->
            <div class="right-content pull-right">
            	
                <!--medical-check-->
            	<div class="medical-check right-content-inner">
                	<form class="form-horizontal upload-form " role="form">
                    	<h1>Please upload <span class="underline">certified</span> copies of the following</h1>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-4 control-label">Profile picture</label>
                          <div class="col-sm-8">
                             <input type="file" data-filename-placement="inside">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-4 control-label">VIT Card</label>
                          <div class="col-sm-8">
                             <input type="file" data-filename-placement="inside">
                             <a class="link-btn" href="javascript:void(0);" >VIT website</a>
                          </div>
                       </div>
                       <h1>100 points of ID</h1>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-4 off-sel-x control-label">Primary document</label>
                          <div class="col-sm-8">
                             <input type="file" data-filename-placement="inside">
                             <a class="link-btn" href="javascript:void(0);" >100 points of ID website</a>
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-4 off-sel-x control-label">Secondary document</label>
                          <div class="col-sm-8">
                             <input type="file" data-filename-placement="inside">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-4 control-label">Criminal record check</label>
                          <div class="col-sm-8">
                             <input type="file" data-filename-placement="inside">
                             <a class="link-btn" href="javascript:void(0);" >Criminal record check website</a>
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-4 control-label">Working with children card check</label>
                          <div class="col-sm-8">
                             <input type="file" data-filename-placement="inside">
                             <a class="link-btn" href="javascript:void(0);" >Working with children website</a>
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-4 control-label">Resume</label>
                          <div class="col-sm-8">
                             <input type="file" data-filename-placement="inside">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-4 control-label">Tax File Declaration</label>
                          <div class="col-sm-8">
                             <input type="file" data-filename-placement="inside">
                             <a class="link-btn" href="javascript:void(0);" >Download Tax Declaration form</a>
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-4 control-label">Bank statement for payments</label>
                          <div class="col-sm-8">
                             <input type="file" data-filename-placement="inside">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-4 control-label">Statutory Declaration</label>
                          <div class="col-sm-8">
                             <input type="file" data-filename-placement="inside">
                             <a class="link-btn" href="javascript:void(0);" >Download Statutory Declaration form</a>
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-4 control-label">Medical Certificates (if applicable)</label>
                          <div class="col-sm-8">
                             <input type="file" data-filename-placement="inside">
                          </div>
                       </div>
                       
                    </form>
                    
                </div>
                <!--medical-check-->
                      <div class="save-cancel-box">
                      	<a class="save-btn" href="javascript:void(0);" >Save and continue</a>
                      	<a href="javascript:void(0);" >Cancel</a>
                        <div class="clearfix"></div>
                      </div>
                
            </div>
            <!-- right-content -->
            
        </div>
    </div>
</div>
  


@stop