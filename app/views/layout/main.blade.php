<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>{{ $title }}</title>

@include('includes.scripts')

</head>
<body>
	<!-- header -->
    @include('includes.header')
    <!-- header -->
    
    <!-- Landing page-->
    @yield('content')
    <!-- Landing page-->
    
    <!-- footer -->
    @include('includes.footer')
    <!-- footer -->
    
</body>
</html>