@extends('layout/main')

@section('content')

<!-- header -->
@include('includes.subHeader')
<!-- header -->




<div class="container-fluid">
	<div class="container">
    	<div class="row main-content">
        
      <!-- left-nav -->
        @include('includes.leftNav')    
        <!-- left-nav -->

            
            
            <!-- right-content -->
            <div class="right-content pull-right">
            	
                <!--medical-check-->
            	<div class="medical-check right-content-inner">
                	<form class="form-horizontal reference-form" role="form">
                    	<h1>Referee One</h1>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">School / company</label>
                          <div class="col-sm-7">
                             <input type="text" class="form-control" id="firstname" placeholder="Example">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">Start date</label>
                          <div class="col-sm-7">
                             <input type="text" class="form-control" id="firstname" placeholder="24th of May 2013">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">End date</label>
                          <div class="col-sm-7">
                             <input type="text" class="form-control" id="firstname" placeholder="24th of May 2014">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">Name of Referee</label>
                          <div class="col-sm-7">
                             <input type="text" class="form-control" id="firstname" placeholder="John Smith">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">Title / position</label>
                          <div class="col-sm-7">
                             <input type="text" class="form-control" id="firstname" placeholder="Example">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">Address</label>
                          <div class="col-sm-7">
                             <input type="text" class="form-control" id="firstname" placeholder="12 Fake Street, CLAYTON 3168">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">Telephone</label>
                          <div class="col-sm-7">
                             <input type="text" class="form-control" id="firstname" placeholder="(03) 9544 1234">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">Email</label>
                          <div class="col-sm-7">
                             <input type="text" class="form-control" id="firstname" placeholder="johnsmith@example.com.au">
                          </div>
                       </div>
                       
                    </form>
                    
                    
                	<form class="form-horizontal reference-form" role="form">
                    	<h1>Referee Two</h1>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">School / company</label>
                          <div class="col-sm-7">
                             <input type="text" class="form-control" id="firstname" placeholder="Example">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">Start date</label>
                          <div class="col-sm-7">
                             <input type="text" class="form-control" id="firstname" placeholder="24th of May 2013">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">End date</label>
                          <div class="col-sm-7">
                             <input type="text" class="form-control" id="firstname" placeholder="24th of May 2014">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">Name of Referee</label>
                          <div class="col-sm-7">
                             <input type="text" class="form-control" id="firstname" placeholder="John Smith">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">Title / position</label>
                          <div class="col-sm-7">
                             <input type="text" class="form-control" id="firstname" placeholder="Example">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">Address</label>
                          <div class="col-sm-7">
                             <input type="text" class="form-control" id="firstname" placeholder="12 Fake Street, CLAYTON 3168">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">Telephone</label>
                          <div class="col-sm-7">
                             <input type="text" class="form-control" id="firstname" placeholder="(03) 9544 1234">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">Email</label>
                          <div class="col-sm-7">
                             <input type="text" class="form-control" id="firstname" placeholder="johnsmith@example.com.au">
                          </div>
                       </div>
                       
                    </form>
                      
                </div>
                <!--medical-check-->
                      <div class="save-cancel-box">
                      	<a class="save-btn" href="javascript:void(0);" >Save and continue</a>
                      	<a href="javascript:void(0);" >Cancel</a>
                        <div class="clearfix"></div>
                      </div>
                
            </div>
            <!-- right-content -->
            
        </div>
    </div>
</div>
  

@stop