@extends('layout/main')

@section('content')

	<!-- header -->
    @include('includes.subHeader')
    <!-- header -->
    


<div class="container-fluid">
	<div class="container">
    	<div class="row main-content">
            
            <!-- right-content -->
            <div class="teacher-landing pull-right">
            	
                <!--medical-check-->
            	<div class="medical-check right-content-inner">
                	<form class="form-horizontal reference-form quals-form" role="form">
                    	<h1>Where do you want to teach? </h1>
                       <div class="form-group">
                          <div class="col-sm-7">
                             <input type="text" class="form-control" id="firstname" placeholder="Address, Suburb, Postcode, or Regions">
                          </div>
                           <div class="dropdown dropdown-wrap">
                              <button class="btn btn-default dropdown-toggle dropdown-btn" type="button" id="dropdownMenu1" data-toggle="dropdown">
                                Travel distance
                                <span class="caret"></span>
                              </button>
                              <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
                                <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
                              </ul>
                           </div>  
                           <a href="javascript:void(0);" class="search-wrap"><span ></span></a>
                                                    	
                       </div>
                       
                    </form>
                    
                    
                    
                </div>
                <!--medical-check-->
                
                	<!--map-content-area-->
					<div class="map-content-area">
                    <div class="map-left-content pull-left">
                        <form class="form-horizontal reference-form teacher-preference-form" role="form">
                            <h1>Where do you want to teach? </h1>
                           <div class="form-group">
                              <div class="col-sm-10 checkBoxOuter">
                                <div class="checkBox"></div>
                                <div class="checkBoxText">Primary</div>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-10 checkBoxOuter">
                                <div class="checkBox"></div>
                                <div class="checkBoxText">Secondary (7-10)</div>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-10 checkBoxOuter">
                                <div class="checkBox"></div>
                                <div class="checkBoxText">Secondary (V.C.E)</div>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-10 checkBoxOuter last-child">
                                <div class="checkBox"></div>
                                <div class="checkBoxText">Special Education</div>
                              </div>
                           </div>
                            <h1>Schools you want to teach at</h1>
                           <div class="form-group">
                              <div class="col-sm-10 checkBoxOuter">
                                <div class="checkBox"></div>
                                <div class="checkBoxText">Government</div>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-10 checkBoxOuter">
                                <div class="checkBox"></div>
                                <div class="checkBoxText">Catholic</div>
                              </div>
                           </div>
                           <div class="form-group">
                              <div class="col-sm-10 checkBoxOuter">
                                <div class="checkBox"></div>
                                <div class="checkBoxText">Independent</div>
                              </div>
                           </div>
                           
                        </form>
                         <a class="create-account" href="teacher-your-details.php" >Create account</a>
                    </div>
                    <div class="right-map pull-right">
                    	
                        
                        <!--map-overlay-->
                    	<div class="map-overlay" style="display:none;">
                        	<div class="map-overlay-content">
                            
                            	<!--map-content-header-->
                            	<div class="map-content-header">
                                	<div class="school-tag pull-left"><img src="assets/images/school-tag.jpg" alt="" /></div>
                                    <div class="school-title pull-left">
                                    	<h3>Trinity Catholic Primary School</h3>
                                        <p><span class="title">Primary</span>  |  Catholic</p>
                                    </div>
                                     <div class="clearfix"></div>
                                </div>
                                <!--map-content-header-->
                               
                                
                                <!--school-information-->
                                <ul class="school-information">
                                	<li>
                                    	<div>Address:</div>
                                        <div>103 - 129 Oakgrove Drive <br>Narre Warren South, 3805</div>
                                    </li>
                                	<li>
                                    	<div>Phone:</div>
                                        <div>(03) 9704 1970</div>
                                    </li>
                                	<li>
                                    	<div>Website:</div>
                                        <div><a href="javascript:void(0);">www.trinitynarre.catholic.edu.au</a></div>
                                    </li>
                                </ul>
                                <!--school-information-->
                                <div class="clearfix"></div>
                                <!--map-content-footer-->
                                <div class="map-content-footer">
                                	<a class="pull-right" href="javascript:void(0);" >Apply to teach here</a>
                                    <div class="clearfix"></div>
                                </div>
                                <!--map-content-footer-->
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <!--map-overlay-->
                        
                        
                    	<img id="map_img" src="assets/images/larg-map.jpg" />
                    </div>
                    </div>
                    <!--map-content-area-->
                    
                    <div class="clearfix"></div>
                    
                     
            </div>
            <!-- right-content -->
            
        </div>
    </div>
</div>


@stop