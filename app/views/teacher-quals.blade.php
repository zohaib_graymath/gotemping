@extends('layout/main')

@section('content')

	<!-- header -->
    @include('includes.subHeader')
    <!-- header -->
    



<div class="container-fluid">
	<div class="container">
    	<div class="row main-content">
        
        
      <!-- left-nav -->
        @include('includes.leftNav')    
        <!-- left-nav -->

            
            
            
            <!-- right-content -->
            <div class="right-content pull-right">
            	
                <!--medical-check-->
            	<div class="medical-check right-content-inner">
                	<form class="form-horizontal reference-form quals-form" role="form">
                       <div class="form-group">
                          <div class="col-sm-10 checkBoxOuter">
                            <div class="checkBox"></div>
                            <div class="checkBoxText">Do you have a current VIT Card?</div>
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-4 control-label">VIT Registration Number</label>
                          <div class="col-sm-6">
                             <input type="text" class="form-control" id="firstname" placeholder="">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">Initial Registration Date</label>
                          <div class="col-sm-6">
                             <input type="text" class="form-control" id="firstname" placeholder="">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">Expiry date</label>
                          <div class="col-sm-6">
                             <input type="text" class="form-control" id="firstname" placeholder="">
                          </div>
                       </div>
                       <div class="form-group">
                          <div class="col-sm-10 checkBoxOuter">
                            <div class="checkBox"></div>
                            <div class="checkBoxText">Do you have a current Criminal Record Check?</div>
                          </div>
                       </div>
                       <div class="form-group">
                          <div class="col-sm-10 checkBoxOuter">
                            <div class="checkBox"></div>
                            <div class="checkBoxText">Do you have a current Work with Children card?</div>
                          </div>
                       </div>
                       <div class="Divider"></div>
                    </form>
                    
                    <form class="form-horizontal reference-form quals-form" role="form">
                       <div class="form-group">
                          <label for="firstname" class="col-sm-4 control-label">University / Institution Attended</label>
                          <div class="col-sm-6">
                             <input type="text" class="form-control" id="firstname" placeholder="">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-4 control-label">Degree obtained</label>
                          <div class="col-sm-6">
                             <input type="text" class="form-control" id="firstname" placeholder="">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-4 control-label">Date of graduation</label>
                          <div class="col-sm-6">
                             <input type="text" class="form-control" id="firstname" placeholder="">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-4 control-label">Years of Teaching Experience</label>
                          <div class="col-sm-6">
                             <input type="text" class="form-control" id="firstname" placeholder="">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-4 control-label">Years of Senior Experience</label>
                          <div class="col-sm-6">
                             <input type="text" class="form-control" id="firstname" placeholder="">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-4 control-label"></label>
                          <div class="col-sm-6">
                             <a class="bold_btn" href="javascript:void(0);">+ Add another University degree</a>
                          </div>
                       </div>
                    <div class="Divider"></div>
                    </form>
                    
                    <form class="form-horizontal reference-form quals-form" role="form">
                    	<h1>Last teaching position</h1>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-4 control-label">School</label>
                          <div class="col-sm-6">
                             <input type="text" class="form-control" id="firstname" placeholder="">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-4 control-label">Start Date</label>
                          <div class="col-sm-6">
                             <input type="text" class="form-control" id="firstname" placeholder="">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-4 control-label">End Date</label>
                          <div class="col-sm-6">
                             <input type="text" class="form-control" id="firstname" placeholder="">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-4 control-label">Class you taught</label>
                          <div class="col-sm-6">
                             <input type="text" class="form-control" id="firstname" placeholder="">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-4 control-label">Address</label>
                          <div class="col-sm-6">
                             <input type="text" class="form-control" id="firstname" placeholder="">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-4 control-label">Telephone</label>
                          <div class="col-sm-6">
                             <input type="text" class="form-control" id="firstname" placeholder="">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-4 control-label">Email</label>
                          <div class="col-sm-6">
                             <input type="text" class="form-control" id="firstname" placeholder="">
                          </div>
                       </div>
                       <div class="form-group">
	                   	 <div class="Divider marginTop"></div>
    	               </div>
                    </form>
 
                    <form class="form-horizontal reference-form quals-form" role="form">
                    	<div class="form-group">
                          <div class="col-sm-10 checkBoxOuter">
                            <div class="checkBox"></div>
                            <div class="checkBoxText">Do you have a current Work with Children card?</div>
                          </div>
                       </div>
                       <div class="form-group">
                          <textarea placeholder="please provide details of all instances" class="form-control textarea" rows="3"></textarea>
                       </div>
                      
                       <div class="form-group">
	                   	 <div class="Divider marginTop"></div>
    	               </div>
                    </form>
                    
                    <form class="form-horizontal reference-form quals-form" role="form">
                    	<h1>Certificates & licences</h1>
                    	<div class="form-group">
                          <div class="col-sm-10 checkBoxOuter">
                            <div class="checkBox"></div>
                            <div class="checkBoxText">First Aid</div>
                          </div>
                       </div>

                    	<div class="form-group">
                          <div class="col-sm-10 checkBoxOuter">
                            <div class="checkBox"></div>
                            <div class="checkBoxText">Mini Bus Licence</div>
                          </div>
                       </div>

                       <div class="form-group">
	                   	 <div class="Divider marginTop"></div>
    	               </div>
                    </form>

                    <form class="form-horizontal reference-form quals-form" role="form">
                    	<p>Are you qualified to teach a language other than English</p>
                       <div class="form-group">
                         <div class="col-sm-4 checkBoxOuter">
                            <div class="checkBox"></div>
                            <div class="checkBoxText">Language one</div>
                          </div>
                          <div class="col-sm-6">
                             <input type="text" class="form-control" id="firstname" placeholder="">
                          </div>
                       </div>
                       <div class="form-group">
                         <div class="col-sm-4 checkBoxOuter">
                            <div class="checkBox"></div>
                            <div class="checkBoxText">Language Two</div>
                          </div>
                          <div class="col-sm-6">
                             <input type="text" class="form-control" id="firstname" placeholder="">
                          </div>
                       </div>
                       
                       
                     
                       
                       <div class="form-group">
	                   	 <div class="Divider marginTop"></div>
    	               </div>
                    </form>
 

                    
                </div>
                <!--medical-check-->
                      <div class="save-cancel-box">
                      	<a class="save-btn" href="javascript:void(0);" >Save and continue</a>
                      	<a href="javascript:void(0);" >Cancel</a>
                        <div class="clearfix"></div>
                      </div>
                     
            </div>
            <!-- right-content -->
            
        </div>
    </div>
</div>
  
@stop