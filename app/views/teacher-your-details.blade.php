<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>{{ $title }}</title>

@include('includes.scripts')

</head>

<body>
<div class="container-fluid welcome-temping-wrap">
	<div class="container">
    	<div class="row">
            <div class="col-md-12 welcome-temping">
            	<a id="close_btn" class="cross_btn" href="javascript:void(0);" ></a>
                <h1>WELCOME TO GO TEMPING!</h1>
                <p>Styling and content are still to come for this page.</p>
            </div>
            <div class="col-md-12">
            	<ul class="steps-wrap">
                	<li>
                    	<div class="icons step1"></div>
                        <h5>STEP ONE</h5>
                        <p>Fill in your account details</p>
                    </li>
                	<li>
                    	<div class="icons step2"></div>
                        <h5>STEP ONE</h5>
                        <p>Download the app</p>
                    </li>
                	<li>
                    	<div class="icons step3"></div>
                        <h5>STEP ONE</h5>
                        <p>You will be approved</p>
                    </li>
                	<li>
                    	<div class="icons step4"></div>
                        <h5>STEP ONE</h5>
                        <p>Start receing job offers</p>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<!-- header -->
@include('includes.header')
<!-- header -->
    
<!-- header -->
@include('includes.subHeader')
<!-- header -->


<div class="container-fluid">
	<div class="container">
    	<div class="row main-content">
        
        <!-- left-nav -->
        @include('includes.leftNav')    
        <!-- left-nav -->

            
            <!-- right-content -->
            <div class="right-content pull-right">
            	
                <!--medical-check-->
            	<div class="medical-check right-content-inner">
                	<form class="form-horizontal reference-form" role="form">
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">Surname</label>
                          <div class="col-sm-7">
                             <input type="text" class="form-control" id="firstname" placeholder="Smith">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">First Name</label>
                          <div class="col-sm-7">
                             <input type="text" class="form-control" id="firstname" placeholder="John">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">Other Names</label>
                          <div class="col-sm-7">
                             <input type="text" class="form-control" id="firstname" placeholder="Johnny">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">Address</label>
                          <div class="col-sm-7">
                             <input type="text" class="form-control" id="firstname" placeholder="12 Fake Street, CLAYTON 3168">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">Mobile</label>
                          <div class="col-sm-7">
                             <input type="text" class="form-control" id="firstname" placeholder="(+61) 418 789 123">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">Email</label>
                          <div class="col-sm-7">
                             <input type="text" class="form-control" id="firstname" placeholder="johnsmith@example.com.au">
                          </div>
                       </div>
                       <div class="form-group smallInput">
                          <label for="firstname" class="col-sm-3 control-label">Date of Birth</label>
                          <div class="col-sm-2">
                             <input type="text" class="form-control" id="firstname" placeholder="24" maxlength="2">
                          </div>
                          <div class="slash">/</div>
                          <div class="col-sm-2">
                             <input type="text" class="form-control" id="firstname" placeholder="05" maxlength="2">
                          </div>
                          <div class="slash">/</div>
                          <div class="col-sm-2">
                             <input type="text" class="form-control" id="firstname" placeholder="2014" maxlength="4">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">Gender</label>
                          <div class="col-sm-2 radioouter">
                            <div class="radio-btn"></div>
                            <div class="checkBoxText">Male</div>
                          </div>
                           <div class="col-sm-2 radioouter">
                            <div class="radio-btn"></div>
                            <div class="checkBoxText">Female</div>
                          </div>
                            
                       </div>
                       
                    </form>
                    
                </div>
                <!--medical-check-->
                     
            </div>
            <!-- right-content -->
            <!-- right-content -->
            <div class="right-content pull-right">
            	
                <!--medical-check-->
            	<div class="medical-check right-content-inner">
                	<form class="form-horizontal reference-form" role="form">
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">Full name</label>
                          <div class="col-sm-7">
                             <input type="text" class="form-control" id="firstname" placeholder="Steve Smith">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">Relationship</label>
                          <div class="col-sm-7">
                             <input type="text" class="form-control" id="firstname" placeholder="Father">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">Address</label>
                          <div class="col-sm-7">
                             <input type="text" class="form-control" id="firstname" placeholder="12 Fake Street, CLAYTON 3168">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">Mobile</label>
                          <div class="col-sm-7">
                             <input type="text" class="form-control" id="firstname" placeholder="(+61) 418 123 456">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">Email</label>
                          <div class="col-sm-7">
                             <input type="text" class="form-control" id="firstname" placeholder="stevesmith@example.com.au">
                          </div>
                       </div>
                       
                       
                    </form>
                    
                </div>
                <!--medical-check-->
                      <div class="save-cancel-box">
                      	<a class="save-btn" href="javascript:void(0);" >Save and continue</a>
                      	<a href="javascript:void(0);" >Cancel</a>
                        <div class="clearfix"></div>
                      </div>
                
            </div>
            <!-- right-content -->
            
        </div>
    </div>
</div>
  

<!-- footer -->
@include('includes.footer')
<!-- footer -->

</body>
</html>
