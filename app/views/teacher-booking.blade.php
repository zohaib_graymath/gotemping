@extends('layout/main')

@section('content')


<!-- header -->
@include('includes.subHeader')
<!-- header -->



<!-- Overlay -->
<div id="feed_back" class="Overlay" style="display:none;">
	<div class="container-fluid">
    	<div class="container popup-form-wrap">
        	<div class="popup-form pull-right">
            	<div class="popup-form-inner">
            	<h1>Feedback Form</h1>
				<form class="form-horizontal reference-form" role="form">
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">Surname</label>
                          <div class="col-sm-8">
                             <input type="text" class="form-control" id="firstname" placeholder="Example School">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">Teacher name</label>
                          <div class="col-sm-8">
                             <input type="text" class="form-control" id="firstname" placeholder="John Smith">
                          </div>
                       </div>
                       <div class="form-group twoInputs">
                          <label for="firstname" class="col-sm-3 control-label">Class</label>
                          <div class="col-sm-4">
                             <input type="text" class="form-control" id="firstname" placeholder="Mathematics">
                          </div>
                          <div class="slash">to</div>
                          <div class="col-sm-4">
                             <input type="text" class="form-control" id="firstname" placeholder="Mathematics">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">Class</label>
                          <div class="col-sm-8">
                             <input type="text" class="form-control" id="firstname" placeholder="Mathematics">
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">General comments</label>
                          <div class="col-sm-8">
                            <textarea placeholder="please provide details of all instances" class="form-control textarea" rows="3"></textarea>
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">Lesson implemented</label>
                          <div class="col-sm-8">
                            <textarea placeholder="Please provide details" class="form-control textarea" rows="3"></textarea>
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">Modifications madeto lesson</label>
                          <div class="col-sm-8">
                            <textarea placeholder="Please provide details" class="form-control textarea" rows="3"></textarea>
                          </div>
                       </div>
                       
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">Progress of students</label>
                          <div class="col-sm-8">
                            <textarea placeholder="Please provide details" class="form-control textarea" rows="3"></textarea>
                          </div>
                       </div>
                       
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">Exemplary students</label>
                          <div class="col-sm-8">
                            <textarea placeholder="Please provide details" class="form-control textarea" rows="3"></textarea>
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">Behavioural concerns</label>
                          <div class="col-sm-8">
                            <textarea placeholder="Please provide details" class="form-control textarea" rows="3"></textarea>
                          </div>
                       </div>
                       
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">Action taken to deal withbehavioural concerns</label>
                          <div class="col-sm-8">
                            <textarea placeholder="Please provide details" class="form-control textarea" rows="3"></textarea>
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">Did you relive any other areas? </label>
                          <div class="col-sm-8">
                            <textarea placeholder="Please provide details" class="form-control textarea" rows="3"></textarea>
                          </div>
                       </div>
                       <div class="form-group">
                          <label for="firstname" class="col-sm-3 control-label">Other notes / comments</label>
                          <div class="col-sm-8">
                            <textarea placeholder="Please provide details" class="form-control textarea" rows="3"></textarea>
                          </div>
                       </div>
                       
                       
                    </form>                
                    </div>
                    <div class="save-cancel-box">
                      	<a class="save-btn" href="javascript:void(0);">Save and continue</a>
                      	<a class="cancel_btn" href="javascript:void(0);">Cancel</a>
                        <div class="clearfix"></div>
                    </div>                    
            </div>
        </div>
    </div>
</div>
<!-- Overlay -->

















<div class="container-fluid">
	<div class="container">
    	<div class="row main-content">
            
            <!-- teacher-profile -->
            <div class="booking-form">
            	
                <!--booking-form-header-->
            	<div class="booking-form-header">
                	<ul class="strip-wrap">
                    	<li class="head">
                        	<!--<div class="plus-div"></div>-->
                        	<div class="Date">Date</div>                            
                        	<div class="School">School</div>                            
                        	<!--<div class="Work-there">Work there again</div>-->
                        	<div class="Rating">Rating</div>
                        	<div class="Feedback">Feedback</div>
                        	<div class="Feedback-from-school">Feedback from school</div>
                        	<!--<div class="Comments">Comments</div>-->
                        </li>
                    	<li>
                        	<!--<div class="plus-div"><a class="plus-sign" href="javascript:void(0);" ></a></div>-->
                        	<div class="Date">11 / 03 / 2014</div>                            
                        	<div class="School">St Jude’s Primary <br>Primary</div>                            
                        	<!--<div class="Work-there">
                            	<div class="pull-left relative-div">
                                    <div class="radio-btn"></div>
                                    <div class="checkBoxText">Yes</div>
                                </div>
                            	<div class="pull-left relative-div">
                                    <div class="radio-btn"></div>
                                    <div class="checkBoxText">No</div>
                                </div>
                            </div>-->
                        	<div class="Rating">
                                     <input type="text" class="form-control col-sm-4" id="firstname" placeholder="9" maxlength="2">
                                     <p>/ 10</p>
                            </div>
                        	<div class="Feedback"><a class="view-feedback" href="javascript:void(0);" >Feedback</a><a class="add-btn" href="javascript:void(0);" >+ Add More</a></div>
                        	<div class="Feedback-from-school"><a class="view-feedback" href="javascript:void(0);" >View feedback</a></div>
                        	<!--<div class="Comments">Comments</div>-->
                            <div class="border_div"></div>
                            <div class="border_div two"></div>
                            <div class="border_div three"></div>
                            <div class="border_div four"></div>
                        </li>
                    	
                    </ul>
                </div>
                <!--booking-form-header-->
                
            	<div class="clearfix"></div>
            </div>
            <!-- teacher-profile -->
            
            
        </div>
    </div>
</div>
  
@stop