@extends('layout/main')

@section('content')
    
    
    <div class="container-fluid landing-bg">
        <div class="container2">
            <div class="row">
                <div class="col-md-12 landing-banner">
                    <h1>Find casual teaching work</h1>
                    <p>See over 2,000 schools across Victoria</p>
                    
                    <!-- Postcode Search Fields-->
                    <div class="inputfiles">
                        <input type="text" placeholder="Enter postcode to see schools in your area" />
                        <a href="teacher-landing.php?page=find_jobs" >Search</a>
                        <a class="btn2" href="{{ URL::to('how-it-works') }}" >How It Works</a>
                    </div>
                    <!-- Postcode Search Fields-->
                    
                </div>
            </div>
        </div>
    </div>
 
    
@stop