@extends('layout/main')

@section('content')

<!-- header -->
@include('includes.subHeader')
<!-- header -->



<div class="container-fluid">
	<div class="container">
    	<div class="row main-content">
        
               
      <!-- left-nav -->
        @include('includes.leftNav')    
        <!-- left-nav -->
            
            
            
            <!-- right-content -->
            <div class="right-content pull-right">
            	
                <!--medical-check-->
            	<div class="medical-check right-content-inner">
                    <div class="approval-div">
                        <h3>You are almost there! </h3>
                      <p>You are almost there! As you are working with children we need to verify your identity.  This will take up to 48 hours. Please press the submit button once all your information is completed.</p>
                    </div>
                      
                </div>
                <!--medical-check-->
                      <div class="save-cancel-box">
                      	<a class="save-btn" href="javascript:void(0);" >Save and continue</a>
                      	<a href="javascript:void(0);" >Cancel</a>
                        <div class="clearfix"></div>
                      </div>
                
            </div>
            <!-- right-content -->
            
        </div>
    </div>
</div>
  


@stop