
<?php
 $how_work = $log_in = $find_job = $teacher = "";

$script_filename = $_SERVER["SCRIPT_FILENAME"];
$page = isset($_GET["page"]) ? $_GET["page"]:"";

if (strpos($script_filename, "how-to-work.php") > 0) {
    $how_work = "active";
}
 else if (strpos($script_filename, "sign-in.php") > 0) {
    $log_in = "active";
}
 else if (strpos($script_filename, "teacher-landing.php") > 0 && $page=="find_jobs") {
    $find_job = "active";
} 
else if (strpos($script_filename, "teacher-landing.php") > 0 && $page=="schools") {
    $teacher = "active";
}
//echo $page;
//exit;
?>






<!-- Creating Account Popup -->
	<div id="sing_in" class="container-fluid signinWrap" style="display:none;">
    	<div id="creat_login_form" class="creat-account">
        	<div class="account-title"><h2>CREATE ACCOUNT</h2></div>
        	<input type="text" placeholder="Email" />
        	<input type="text" placeholder="Password" />
        	<input type="text" placeholder="Confirm Password" />
            <a class="creatbtn" href="javascript:void(0);" >Create account</a>
            <div class="alreadyMember"><p>Already a member</p></div>
            <a id="creat_sign_in" class="creatbtn singinbtn" href="javascript:void(0);" >Sign in</a>
        </div>

		<div id="sing_in_form" class="creat-account" style="display:none;">
        	<div class="account-title"><h2>CREATE ACCOUNT</h2></div>
        	<input type="text" placeholder="Email" />
        	<input type="text" placeholder="Password" />
            <div class="checkbox keepMe">
                  <div class="col-sm-10 checkBoxOuter">
                    <div class="checkBox"></div>
                    <div class="checkBoxText">Keep me logged in</div>
                  </div>
           </div>
           <div class="clearfix"></div>
            <a class="creatbtn" href="javascript:void(0);" >Sign in</a>
            <div class="forgetpswd"><a class="forgotP" href="javascript:void(0);" >I forgot my password</a></div>
            <div class="alreadyMember notmember"><p>Not a member</p></div>
            <a id="creat_account" class="creatbtn singinbtn" href="javascript:void(0);" >Create an account</a>
        </div>
        
    </div>
<!-- Creating Account Popup -->




<div class="navbar navbar-default nav-wrap navbar-static-top" role="navigation">
      <div class="container2">
        <div class="navbar-header pull-left">
          <a class="navbar-brand logo" href="{{ URL::to('') }}"><img src="assets/images/logo.jpg" alt="" /></a>
        </div>
        <!--<div class="navbar-collapse collapse"> -->
          <!--<ul class="nav navbar-nav">
            <li class="active"><a href="#">Home</a></li>
            <li><a href="#about">About</a></li>
            <li><a href="#contact">Contact</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu">
                <li><a href="#">Action</a></li>
                <li><a href="#">Another action</a></li>
                <li><a href="#">Something else here</a></li>
                <li class="divider"></li>
                <li class="dropdown-header">Nav header</li>
                <li><a href="#">Separated link</a></li>
                <li><a href="#">One more separated link</a></li>
              </ul>
            </li>
          </ul>
          <!--<ul class="nav navbar-nav navbar-right">
            <li><a href="../navbar/">Schools</a></li>
            <li class="active"><a href="./">How it works</a></li>
            <li><a href="">Find Jobs</a></li>
            <li><a href="">Log In</a></li>
          </ul>
        </div>-->
        <!--/.nav-collapse -->
        <a class="mob-btn" href="javascript:void(0);" ><span></span><span></span><span></span></a>
        <div class="Menu">
          <ul class="Menulist">
            <li><a class="<?php echo $teacher; ?>" href="{{ URL::to('schools') }}">Schools</a></li>
            <li><a class="<?php echo $how_work; ?>" href="{{ URL::to('how-it-works') }}">How it works</a></li>
            <li><a class="<?php echo $find_job; ?>"href="{{ URL::to('find-jobs') }}">Find Jobs</a></li>
            <li><a id="sing_in_btn" class="login" href="javascript:void(0);">Log In</a></li>
          </ul>
        
        </div>
        
      </div>
    </div>

