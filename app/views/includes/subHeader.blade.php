<?php

$teacher_pre = $teacher_details = $teacher_booking = $teacher = $teacher_account = "";

$page_route = Request::url() ;

$script_filename = $page_route ; //$_SERVER["SCRIPT_FILENAME"];

if (strpos($script_filename, "profile") > 0) {
    $teacher_pre = "active";
}
 else if (strpos($script_filename, "your-details") > 0) {
    $teacher_details = "active";
}
else if (strpos($script_filename, "booking") > 0) {
    $teacher_booking = "active";
}
 else if (strpos($script_filename, "landing") > 0) {
    $find_job = "active";
} 
 else if (strpos($script_filename, "account-setting") > 0) {
    $teacher_account = "active";
} 

//exit;
?>






<div class="container-fluid sub-nav-wrap">
	<div class="container">
    	<ul class="subNav pull-left">
        	<li><a class="<?php echo $teacher_details ?>" href="{{ URL::to('your-details') }}" >Profile</a></li>
        	<li><a class="<?php echo $teacher_booking ?>" href="{{ URL::to('booking') }}" >Bookings</a></li>
        	<li><a class="<?php echo $teacher_pre ?>" href="{{ URL::to('profile') }}" >View Profile</a></li>
        	<li><a class="<?php echo $teacher_account ?>" href="{{ URL::to('account-settings') }}" >Account Settings</a></li>
        </ul>
    </div>
</div>
