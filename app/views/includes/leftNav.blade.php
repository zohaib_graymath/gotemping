<?php
 $tab1 = $tab2 = $tab3 = $tab4 = $tab5 = $tab6 = $tab7 = $tab8 = $tab9 = "";

$page_route = Request::url() ;

$script_filename = $page_route ; //$_SERVER["SCRIPT_FILENAME"];

if (strpos($script_filename, "your-details") > 0) {
    $tab1 = "active";
}
 else if (strpos($script_filename, "preferences") > 0) {
    $tab2 = "active";
}
 else if (strpos($script_filename, "qualifications") > 0) {
    $tab3 = "active";
}
 else if (strpos($script_filename, "references") > 0) {
    $tab4 = "active";
}
 else if (strpos($script_filename, "medical") > 0) {
    $tab5 = "active";
}
 else if (strpos($script_filename, "uploads") > 0) {
    $tab6 = "active";
}
 else if (strpos($script_filename, "banking") > 0) {
    $tab7 = "active";
}
 else if (strpos($script_filename, "submit-for-approval") > 0) {
    $tab8 = "active";
}
 else if (strpos($script_filename, "start-getting-job") > 0) {
    $tab9 = "active";
}

//exit;
?>




        	<!-- sidebar -->
        	<div class="sidebar pull-left">
                <!-- Left Menu -->
                <div class="left-nav-wrap pull-left">
                    <ul class="left-nav">
                        <li><a class="<?php echo $tab1 ?>" href="{{ URL::to('your-details') }}" >Your Details</a></li>
                        <li><a class="<?php echo $tab2 ?>" href="{{ URL::to('preferences') }}" >Teaching Preferences</a></li>
                        <li><a class="<?php echo $tab3 ?>" href="{{ URL::to('qualifications') }}" >Qualifications</a></li>
                        <li><a class="<?php echo $tab4 ?>" href="{{ URL::to('references') }}" >References</a></li>
                        <li><a class="<?php echo $tab5 ?>" href="{{ URL::to('medical') }}" >Medical Check</a></li>
                        <li><a class="<?php echo $tab6 ?>" href="{{ URL::to('uploads') }}" >Uploads</a></li>
                        <li><a class="<?php echo $tab7 ?>" href="{{ URL::to('banking') }}" >Banking & Super</a></li>
                        <li><a class="<?php echo $tab8 ?>" href="{{ URL::to('submit-for-approval') }}" >Submit for Approval</a></li>
                    </ul>
                </div>
                <!-- Left Menu -->
                <div class="left-nav-wrap pull-left">
                    <ul class="left-nav">
                        <li><a class="<?php echo $tab9 ?>" href="{{ URL::to('start-getting-job') }}" >Start Getting Jobs</a></li>
                    </ul>
                 </div>
            </div>
            <!-- sidebar -->
