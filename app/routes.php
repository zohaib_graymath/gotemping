<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/


/*
|-------------------------------
|	Main Header links routing
|-------------------------------
*/

Route::get('/', function()
{
	return View::make('index')->with('title','GoTemping');
});

Route::get('how-it-works', function()
{
	return View::make('how-it-works')->with('title','How It Works');
});

Route::get('schools', function()
{
	return View::make('teacher-landing')->with('title','Schools');
});

Route::get('find-jobs', function()
{
	return View::make('teacher-landing')->with('title','Find Jobs');
});


/*
|-------------------------------
|	Sub Header links routing
|-------------------------------
*/


Route::get('booking', function()
{
	return View::make('teacher-booking')->with('title','Booking');
});

Route::get('your-details', function()
{
	return View::make('teacher-your-details')->with('title','Your Details');
});

Route::get('profile', function()
{
	return View::make('teacher-profile')->with('title','View Profile');
});

Route::get('account-settings', function()
{
	return View::make('teacher-account-setting')->with('title','Account Settings');
});


/*
|-------------------------------
|	Left Navigation bar routing
|-------------------------------
*/


Route::get('preferences', function()
{
	return View::make('teacher-preference')->with('title','Teacher Preferences');
});

Route::get('qualifications', function()
{
	return View::make('teacher-quals')->with('title','Qualifications');
});

Route::get('references', function()
{
	return View::make('teacher-reference')->with('title','References');
});

Route::get('medical', function()
{
	return View::make('teacher-medical')->with('title','Medical');
});

Route::get('uploads', function()
{
	return View::make('teacher-uploads')->with('title','Uploads');
});

Route::get('banking', function()
{
	return View::make('teacher-banking')->with('title','Banking');
});

Route::get('submit-for-approval', function()
{
	return View::make('teacher-submit-for-approval')->with('title','Submit for approval');
});

Route::get('start-getting-job', function()
{
	return View::make('start-getting-job')->with('title','Start Getting Job');
});
